#pragma once
#include "vektor.cpp"
#include <math.h>
#include <cstdlib>

#define PI 3.14159

class ball{
public:

	static int MAX_VELOCITY;
	static int MAX_RADIUS;
	vektor p,v,a;
	float r,m;
	
	class color{
	public:
		float r,g,b,a;
		color(){
			r=g=b=a=0.0;
		}
	} c;
	ball(){
	}
	ball(int w,int h){
		p=vektor(rand()%w,rand()%h);
		v=vektor(rand()%MAX_VELOCITY,rand()%MAX_VELOCITY);
		r = rand()%MAX_RADIUS + 20.0;
		m=(r*r)/1000;
		
		c.r= rand()/((double)RAND_MAX + 1.0);
		c.g= rand()/((double)RAND_MAX + 1.0);
		c.b= rand()/((double)RAND_MAX + 1.0);
		c.a= rand()/(	(double)RAND_MAX + 1.0);

	}

	void draw(){

		glColor3f(c.r,c.g,c.b);
		
		glBegin(GL_TRIANGLE_FAN);
		glVertex2f(p.x,p.y);
 		for(int i=0;i<101;++i){
 			glVertex2f(p.x+r*cos(2*PI*i/100.0),p.y+r*sin(2*PI*i/100.0));
 		}
 		glEnd();
	}
};
int ball::MAX_VELOCITY = 1000;
int ball::MAX_RADIUS = 30;
