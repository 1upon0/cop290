#pragma once
#include "thread_mgmt.cpp"

static pthread_mutex_t display_m;
static pthread_barrier_t display_b;
static int N;
static vector<ball_manager*> balls;
static vector<wall> walls(4);
class window
{
public:
	static int wWidth;
	static int wHeight;
	static float ASPECT_RATIO;
	window(){
	}
	static void openWindow(int argc,char **argv){
		glutInit(&argc,argv);
		glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGBA);
		glutInitWindowSize(wWidth,wHeight);
		glutInitWindowPosition(50,50);

		glutCreateWindow("Screen Saver");
		//glutFullScreen();

		glClearColor (0.0, 1.0, 0.0, 0.0);

		glMatrixMode(GL_PROJECTION);
		glLoadIdentity();
		glOrtho(0, wWidth, 0, wHeight, -1.0, 1.0);
	}

	static void display()
	{
		glClear(GL_COLOR_BUFFER_BIT);

		//TODO: Body of the screen saver
		for(int i=0;i<N;i++){
			balls[i]->pushJob(job(JOB_INTEGRATE));
		}
		
		usleep(1000);
		
		pthread_barrier_wait(&display_b);
		

		for(int i=0;i<N;i++){
			balls[i]->draw();
		}
		
		glutSwapBuffers();
		
		for(int i=0;i<N-1;i++){
			for(int j=i+1;j<N;j++)
				balls[i]->pushJob(job(JOB_COLLIDEA,balls[j]));
		}
		
		for(int i=0;i<N;i++)
			for(int k=0;k<walls.size();k++)
					balls[i]->pushJob(job(JOB_COLLIDEB,walls[k]));	

	}

	static void resize(int width, int height){
		wHeight = height;
		wWidth = width;

		glViewport(0,0,width,height);
		glMatrixMode(GL_PROJECTION);
		glLoadIdentity();
		glOrtho(0,width,0,height,-1.0,1.0);

		makeWalls(width,height);

	}
	static void makeWalls(int width,int height)
	{
		vektor xx(1,0),yy(0,1);
		walls[0].n = xx;
		walls[1].n = yy;
		walls[2].n = xx*(-1);
		walls[3].n = yy*(-1);
		walls[2].d = width;
		walls[3].d = height;	
		
	}
	static void start(const int& argc,char **argv)
	{
		openWindow(argc,argv);
		//TODO
		N=50;
		makeWalls(wWidth,wHeight);
		
		ball_manager::initLocks(&display_m,&display_b,N);
		for(int i=0;i<N;i++){
			ball b(wWidth,wHeight);
			balls.push_back(ball_manager::create(b,&display_m,&display_b));
		}
		glutDisplayFunc(window::display);
		glutIdleFunc(window::display);
		glutReshapeFunc(window::resize);
		glutMainLoop();
	}
};
int window::wWidth=600;
int window::wHeight=500;
float window::ASPECT_RATIO=1.2;


