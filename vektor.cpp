#pragma once
#ifndef VEKTOR
#define VEKTOR
#include <iostream>
#include <cmath>
using namespace std;

 class vektor{
	public:
	float x;
	float y;
	vektor(){x = y = 0;}
	vektor(float xx,float yy){ x=xx; y=yy;}

	double dist(vektor b){
	 float xx = x-b.x,yy=y-b.y;
	return sqrt(xx*xx+yy*yy);
	}

	vektor operator+(const vektor &v){
		return vektor(x+v.x,y+v.y);
	}

	vektor operator-(const vektor &v){
		return vektor(x-v.x,y-v.y);
	}
	
	vektor operator*(double k){
		return vektor(x*k,y*k);
	}
	vektor operator/(double k){
		return vektor(x/k,y/k);
	}
	float operator*(const vektor &v){
		return (x*v.x+y*v.y);
	}
	
	void operator+=(const vektor&v){
		x+=v.x;
		y+=v.y;
	}
	void operator-=(const vektor &v){
		x -= v.x;
		y -= v.y;
	}
	void operator*=(const vektor  &v){

		x *= v.x;
		y *= v.y;
	}
	void operator*=(double k){
		x *= k;
		y *= k;
	}
	void operator/=(double k){
		if(-1e-8 < k < 1e-8){
			cout<<"Cannot divide by 0"<<endl;
			return;
		}
		x /= k;
		y /= k;
	}

	float magsq(){
		return (x*x+y*y);
	}	
	double mag(){
		return sqrt(magsq());
	}
	void normalize(){
		float magg = mag();
		//cout<<magg<<flush;
		if(magg == 0)
			return;
		
		x/=magg;
		y/=magg;
	}
	vektor print(){
		cout<<x<<" "<<y<<endl;
	}
	vektor normalized(){
		vektor a(x,y);
		a.normalize();
		return a;
	}
};


vektor operator*(double t,vektor &v){
	return v*t;
}
#endif
