#include <iostream>
#include <vector>
#include <algorithm>
#include <cstdlib>
#include <cstdio>
#include <ctime>
#include <unistd.h>
#include <GL/glut.h>

#include "window.cpp"

using namespace std;


int main(int argc,char **argv)
{	
	window::start(argc,argv);
	return 0;
}