#pragma once
#include "vektor.cpp"
#include "ball.cpp"
#include "wall.cpp"

class phy
{
public:
	static void solveCollision(ball &b1,ball &b2){

		vektor n = b2.p - b1.p;
		float u1 = b1.v * n, u2 = b2.v * n;

		if(u1-u2<0)
			return;

		float dist = n.magsq();
		if(dist>(b1.r + b2.r)*(b1.r + b2.r))
			return;

		dist=sqrt(dist);
		u1/=dist;
		u2/=dist;
		n=n/dist;

		float v1,v2,vcm=(u1*b1.m+u2*b2.m)/(b1.m+b2.m);
		v1=vcm+((u2-u1)*b2.m)/(b1.m+b2.m);
		v2=vcm+((u1-u2)*b1.m)/(b1.m+b2.m);
		b1.v=b1.v+(v1-u1)*n;
		b2.v=b2.v+(v2-u2)*n;

		float pen=b1.r+b2.r-dist;
		b1.p=b1.p-n*pen*b2.m/(b1.m+b2.m);
		b2.p=b2.p+n*pen*b1.m/(b1.m+b2.m);
	}


	
	static void integrate(ball &b,float dt = 1.0){
		b.p = b.p+ b.v*dt;
		b.v =b.v+ b.a*dt;
	}


	static void solveCollision(ball &b,wall *w){
		vektor n = w->n;
		float u = b.v*n , d = w->d;
		if(u >=0)return;
		
		float dist=b.p*n+d-b.r;
		if(dist>0)return;
		b.v = b.v -2.0*u*n;
		b.p=b.p-n*dist;
	}
};
