#pragma once
#include <queue>
#include <pthread.h>
#include <unistd.h>
#include "ball.cpp"
#include "physics.cpp"

#define JOB_INTEGRATE	1
#define JOB_COLLIDEA 	2
#define JOB_COLLIDEB    4
#define JOB_EXIT 		3

class ball_manager;
class job{
public:
	int type;
	ball_manager *b;
	wall *w;
	job(int _type){b=NULL;type=_type;w=NULL;}
	job(int _type,ball_manager *_b){b=_b;type=_type;w=NULL;}
	job(int _type,wall& _w){b=NULL;type=_type;w=&_w;}
};
class ball_manager{
	pthread_t thread;
	queue<job> jobs;
	pthread_mutex_t queue_m,ball_m,*display_m;
	pthread_cond_t	queue_cv;
	pthread_attr_t thread_attr;
	pthread_barrier_t *display_b;
	ball_manager(){
		display_m=NULL;display_b=NULL;
		pthread_mutex_init(&queue_m,NULL);
		pthread_mutex_init(&ball_m,NULL);
		pthread_cond_init(&queue_cv,NULL);
	}
	void loop(){

		while(1)
		{
			//cout<<"j"<<flush;
			pthread_mutex_lock(&queue_m);
			while(jobs.empty()){
				pthread_cond_wait(&queue_cv,&queue_m);	
			}
			
			job j=jobs.front();
			jobs.pop();
			
			pthread_mutex_unlock(&queue_m);
			if(j.type==JOB_EXIT)
				break;
			if(j.type==JOB_COLLIDEA)
				collide(j.b);

			else if(j.type == JOB_COLLIDEB)
					collide(j.w);
				 else if(j.type==JOB_INTEGRATE)
						integrate();
		}
  		pthread_exit(NULL);
	}
	
	void integrate(){
		pthread_mutex_lock(&ball_m);
		phy::integrate(b,0.01);
		//pthread_mutex_lock(display_m);
		//b.draw();
		//cout<<"d"<<flush;
		//pthread_mutex_unlock(display_m);
		pthread_mutex_unlock(&ball_m);
		pthread_barrier_wait(display_b);
		//cout<<"B"<<flush;
	}
	void collide(ball_manager *bm){
		pthread_mutex_lock(&ball_m);
		//phy::solveCollision(b);
		pthread_mutex_lock(&(bm->ball_m));
		phy::solveCollision(b,bm->b);
		pthread_mutex_unlock(&(bm->ball_m));
		pthread_mutex_unlock(&ball_m);
	}
	void collide(wall *w){
		pthread_mutex_lock(&ball_m);
		//pthread_mutex_lock(&(w->wall_m));

		phy::solveCollision(b,w);
		pthread_mutex_unlock(&ball_m);
		//pthread_mutex_unlock(&(w->wall_m));	
	}
public:
	ball b;
	
	~ball_manager(){
		pushJob(job(JOB_EXIT));
		pthread_join(thread,NULL);

		pthread_attr_destroy(&thread_attr);
		pthread_mutex_destroy(&queue_m);
		pthread_mutex_destroy(&ball_m);
		pthread_cond_destroy(&queue_cv);
	}

	void draw(){
		pthread_mutex_lock(&ball_m);
		//b.integrate(0.01);
		pthread_mutex_lock(display_m);
		b.draw();
		//cout<<"d"<<flush;
		pthread_mutex_unlock(display_m);
		pthread_mutex_unlock(&ball_m);
		//pthread_barrier_wait(display_b);
		//cout<<"B"<<flush;
	}

	void pushJob(const job &j){
		pthread_mutex_lock(&queue_m);
	 	jobs.push(j);
	 	if(jobs.size()==1)
	 		pthread_cond_signal(&queue_cv);
	 	pthread_mutex_unlock(&queue_m);
	}

	static void* init_thread(void *bm){
		((ball_manager*)bm)->loop();
	}
	
	static ball_manager* create(const ball &b,pthread_mutex_t *display_mutex,pthread_barrier_t *display_barrier){
		ball_manager *bm=new ball_manager();
		bm->b=b;
		bm->display_m=display_mutex;
		bm->display_b=display_barrier;
		pthread_attr_init(&bm->thread_attr);
	  	pthread_attr_setdetachstate(&bm->thread_attr, PTHREAD_CREATE_JOINABLE);

		int res=pthread_create(&bm->thread, &bm->thread_attr, ball_manager::init_thread, bm);
		if(res)
			throw "Thread creation failed";
		return bm;
	}
	static void initLocks(pthread_mutex_t *display_mutex,pthread_barrier_t *display_barrier,int n){
		pthread_mutex_init(display_mutex,NULL);
		pthread_barrier_init(display_barrier,NULL,n+1);
	}
	static void destroyLocks(pthread_mutex_t *display_mutex,pthread_barrier_t *display_barrier){
		pthread_mutex_destroy(display_mutex);
		pthread_barrier_destroy(display_barrier);
	}
};
